package com.iy.housekeepingapi.controller;

import com.iy.housekeepingapi.model.HousekeepingRequest;
import com.iy.housekeepingapi.service.HousekeepingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/housekeeping")
public class HousekeepingController {
    private  final HousekeepingService housekeepingService;

    @PostMapping("/ledger")
    public String setHousekeeping(@RequestBody HousekeepingRequest request){
        housekeepingService.setHousekeeping(request);

        return "OK";



    }
}
