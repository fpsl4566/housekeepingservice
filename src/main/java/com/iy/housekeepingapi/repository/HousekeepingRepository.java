package com.iy.housekeepingapi.repository;

import com.iy.housekeepingapi.entity.Housekeeping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HousekeepingRepository extends JpaRepository <Housekeeping, Long> {
}
