package com.iy.housekeepingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor


public enum AmountType {
    FOOD("식비",false),
    SUBWAY("교통비",false),
    HOSPITAL("병원비",false);

    private final String Income;
    private final Boolean Spending;

}
