package com.iy.housekeepingapi.service;

import com.iy.housekeepingapi.entity.Housekeeping;
import com.iy.housekeepingapi.model.HousekeepingRequest;
import com.iy.housekeepingapi.repository.HousekeepingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HousekeepingService {
    private final HousekeepingRepository housekeepingRepository;

    public void setHousekeeping(HousekeepingRequest request){
        Housekeeping addDate = new Housekeeping();
        addDate.setDateMake(LocalDate.now());
        addDate.setInout(request.getInout());
        addDate.setAmountType(request.getAmountType());
        addDate.setEtcMemo(request.getEtcMemo());
        housekeepingRepository.save(addDate);
 }
}
