package com.iy.housekeepingapi.entity;

import com.iy.housekeepingapi.enums.AmountType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Housekeeping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateMake;

    @Column(nullable = false, length = 20)
    private String inout;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private AmountType amountType;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;



}
