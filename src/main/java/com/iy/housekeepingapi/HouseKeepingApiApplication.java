package com.iy.housekeepingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HouseKeepingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HouseKeepingApiApplication.class, args);
	}

}
