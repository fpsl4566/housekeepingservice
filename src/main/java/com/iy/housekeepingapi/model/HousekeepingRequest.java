package com.iy.housekeepingapi.model;

import com.iy.housekeepingapi.enums.AmountType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class HousekeepingRequest {
    private String inout;
    private Integer amount;

    @Enumerated(value = EnumType.STRING)
    private AmountType amountType;
    private String etcMemo;
}
